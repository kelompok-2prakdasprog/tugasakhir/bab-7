Nama	: Deden Naufal Khairul
Kelas	: B� Teknik Informatika 

Bab 7 
8.	Dalam bidang pengolahan citra (image processing), elemen gambar terkecil disebut pixel (picture element). Nilai pixel untuk gambar 256  warna adalah dari 0 sampai 255.  Operasi operasi terhadap pixel seringkali berada di luar rentang nilai ini. Jika in kasusnya, maka nilai hasil operasi harus di potong (clipping) sehingga tetap berada di dalam interval [0...255]. Jika nilai hasil operasi lebih besar dari 255, maka nilai tersebut di potong menjadi 255, dan bila negatif maka potongan menjadi 0. di baca sebuah nilai hasil operasi pengolahan citra, buatlah algoritma untuk melakukan clipping tersebut.

Jawab :

read(HasiOperasi)
if HasilOperasi > 255 then
   Akhir <-- 255
else
    if HasilOperasi < 0 then
       Akhir <-- 0
    else
       Akhir <-- HasilOperasi
    endif
endif
write(Akhir)
