#include <iostream>
#include <conio.h>
using namespace std;
main(){
    int a;
    menu:
    cout<<"Masukkan Bilangan Bulat Positif : "; cin>>a;
    if(a>0){
        if(a%4==0) cout<<a<<" adalah bilangan positif kelipatan 4"<<endl;
        else{
            cout<<a<<" adalah bukan bilangan positif atau bukan kelipatan 4"<<endl;
            getch();
            goto menu;
        }
    }else{
        cout<<"ERROR"<<endl;
        cout<<"INPUT A POSITIVE NUMBER"<<endl;
        getch();
        goto menu;
    }
    getch();
}